FROM ubuntu

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

# RUN apt-get update
# RUN apt-get install -y software-properties-common
# RUN sed -i "/^# deb.*multiverse/ s/^# //" /etc/apt/sources.list
RUN apt-get update -y
RUN apt-get install -y software-properties-common
RUN add-apt-repository multiverse
RUN apt-get -y install \
    python \
    pip \
    python2 \
    git \
    sudo \
    boxes \
    figlet \
    lolcat \
    gcc \
    make \
    g++ \
    ruby-dev \
    libpcap-dev \
    libusb-1.0-0-dev \
    libnetfilter-queue-dev \
    net-tools \
    iproute2  \
    nmap \
    ruby \
    build-essential \
    wget

COPY requirements.txt .
RUN wget https://bootstrap.pypa.io/pip/2.7/get-pip.py
RUN python2 get-pip.py
RUN python2 -m pip install -r requirements.txt
RUN python3 -m pip install -r requirements.txt
# RUN ln -sf /usr/bin/python2.7 /usr/bin/python

# HOMEBREW
# RUN useradd -m -s /bin/bash linuxbrew
# RUN echo 'linuxbrew ALL=(ALL) NOPASSWD:ALL' >>/etc/sudoers
# USER linuxbrew
# RUN git clone https://github.com/Homebrew/brew.git /home/linuxbrew/.linuxbrew/Homebrew
# WORKDIR /home/linuxbrew/.linuxbrew
# RUN mkdir -p bin etc include lib opt sbin share var/homebrew/linked Cellar
# RUN ln -s ../Homebrew/bin/brew /home/linuxbrew/.linuxbrew/bin/
# ENV PATH=/home/linuxbrew/.linuxbrew/bin:/home/linuxbrew/.linuxbrew/sbin:$PATH \
# 	SHELL=/bin/bash \
# 	USER=linuxbrew
# RUN HOMEBREW_NO_ANALYTICS=1 HOMEBREW_NO_AUTO_UPDATE=1 brew tap homebrew/core \
# 	&& rm -rf ~/.cache
# RUN brew install bettercap

RUN apt-get -y install golang

RUN go get -u -v github.com/bettercap/bettercap
ENV PATH="$PATH:/root/go/bin"

WORKDIR /app
COPY . /app

USER root

ENTRYPOINT ["python3", "hackingtool.py"]
# ENTRYPOINT ["bash"]